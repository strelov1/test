<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Транзакции';
$this->params['breadcrumbs'][] = $this->title;

$status = ['в обработке', 'успешно', 'не удачно'];

?>
<div class="transaction-index">

    <?php Pjax::begin(['id' => 'transaction']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'description',
            'ttl',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) use ($status) {
                    switch ($model->status) {
                        case '1':
                            $class = 'success';
                            break;
                        case '0':
                            $class = 'info';
                            break;
                        case '2':
                            $class = 'danger';
                            break;
                        default:
                            $class = 'default';
                    };
                    return Html::tag('span', Html::encode($status[$model->status]), ['class' => 'status label label-'.$class]);
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    $status,
                    ['class' => 'form-control', 'prompt' => '']
                ),
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>


</div>
