<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Транзакции';
$this->params['breadcrumbs'][] = $this->title;

$status = ['в обработке', 'успешно', 'не удачно'];

/* Асинхронный запрос обновление статуса */
$this->registerJs(
    "$(document).on('click', '.refresh_status', function(){
    	status_id = $(this).closest('tr').data('key')
		$.ajax({
            url: '".yii\helpers\Url::toRoute("transaction/status-update")."',
            data: {id: status_id},
            success: function (data){
                //alert(data);
                $.pjax.reload({container:\"#transaction\"});
            },
		});
	});
	"
);
?>
<div class="transaction-index">

    <p>
        <?= Html::a('Создать транзакцию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(['id' => 'transaction']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'description',
            'ttl',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) use ($status) {
                    switch ($model->status) {
                        case '1':
                            $class = 'success';
                            break;
                        case '0':
                            $class = 'info';
                            break;
                        case '2':
                            $class = 'danger';
                             break;
                        default:
                            $class = 'default';
                    };
                    return Html::tag('span', Html::encode($status[$model->status]), ['class' => 'status label label-'.$class]);
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    $status,
                    ['class' => 'form-control', 'prompt' => '']
                ),
            ],
            [
                'format' => 'raw',
                'value' => function ($model) use ($status) {
                    return ($model->status == 0) ? Html::a('', 'javascript:void(0)',
                        ['class' => 'glyphicon glyphicon-refresh refresh_status'],
                        ['title' =>  'Refresh',]) : '';
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>


</div>
