<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    ['label' => 'Пользователи', 'icon' => 'fa fa-user', 'url' => ['/user'], 'visible' => isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['admin'])],
                    ['label' => 'Все транзакции', 'icon' => 'fa fa-user', 'url' => ['/transaction/all'], 'visible' => isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['admin'])],
                    ['label' => 'Транзакции', 'icon' => 'fa fa-dashboard', 'url' => ['/transaction/index'], 'visible' => isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['user'])],
                   ],
            ]
        );?>
    </section>

</aside>
