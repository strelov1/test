<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        //Добавляем роль "user"
        $user = $auth->createRole('user');
        $auth->add($user);

        //Добавляем роль "admin"
        $admin = $auth->createRole('admin');
        $auth->add($admin);

    }
}