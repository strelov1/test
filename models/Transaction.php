<?php

namespace app\models;

use Yii;
use app\models\User;

/**
 * This is the model class for table "transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 * @property string $description
 * @property string $ttl
 */
class Transaction extends \yii\db\ActiveRecord
{
    const APP_URL = 'http://m.oneclickmoney.com/test/index.php?r=request';
    const APP_KEY = 'DH3qrx8ZolZR';
    const STATUS_WAIT = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_FAIL = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ttl'], 'required'],
            [['user_id', 'status', 'ttl'], 'integer'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID Транзакции',
            'user_id' => 'Пользователь',
            'status' => 'Статус',
            'description' => 'Описание',
            'ttl' => 'Время',
        ];
    }

    public function getStatus($id)
    {
        $response = '<xml>
                        <action>status</action>
                        <app>'.self::APP_KEY.'</app>
                        <transaction id="'.$id.'" />
                     </xml>';

        $curl = curl_init(self::APP_URL);
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $response,
            ]);

        $xml = simplexml_load_string(curl_exec($curl));

        var_dump($xml);

        switch ($xml->transaction["status"]) {
            case '0':
                return $this->updateStatus(self::STATUS_WAIT);
                break;
            case '1':
                return $this->updateStatus(self::STATUS_SUCCESS);
                break;
            case '2':
                return $this->updateStatus(self::STATUS_FAIL);
                break;
        }

    }

    public function updateStatus($status)
    {
        return \Yii::$app->db->createCommand()->update(self::tableName(), ['status' => $status], ['id' => $this->id])->execute();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->user_id = Yii::$app->user->id;
            }
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        $response = '<xml>
                        <action>send</action>
                        <app>'.self::APP_KEY.'</app>
                        <transaction id="'.$this->id.'" ttl="'.$this->ttl.'" status="'.$this->status.'" description="'.$this->description.'" />
                        <user>
                            <id>'.$this->user_id.'</id>
                            <name>'.$this->user->username.'</name>
                        </user>
                    </xml>';

        $curl = curl_init(self::APP_URL);
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $response,
            ]);

        $xml = simplexml_load_string(curl_exec($curl));

        /*
         * echo($xml->response["status"]);
         * echo($xml->response["code"]);
         * echo($xml->response["description"]);
        */

        /* Ошибка при добавлении транзации */
        if ($xml->response["code"] == '-1'){
            $this->updateStatus(self::STATUS_FAIL);
        } else {
            $this->updateStatus(self::STATUS_WAIT);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
