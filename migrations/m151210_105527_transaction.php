<?php

use yii\db\Schema;
use yii\db\Migration;

class m151210_105527_transaction extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%transaction}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'description' => $this->string(),
            'ttl' => $this->string()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk-user_id-user_id', '{{%transaction}}', 'user_id', '{{%user}}', 'id', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%transaction}}');
    }
}
